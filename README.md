# Clasificación de genomas completos de Arbovirus: Dengue, Zika y Chikungunya empleando CGR y Regresión Logística
Javier de León, Pablo Hernandez, Luis Pablo Kuri



## Resumen
Utilizando el punto medio y la métrica euclidiana, se realizó la representación del CGR de al menos 50 muestras del genoma completo de los virus dengue, zika y chikungunya (no se realizaron distinciones entre las cepas virales ni serotipos en el caso de dengue). Luego se utilizó un modelo de regresión logística que realizó comparaciones a pares entre los genomas. Esta herramienta de clasificación fue montada sobre un servidor web al cual es posible acceder para verificar la procedencia de un genoma del que se desconozca su clasificación mediante un archivo FASTA. Debido al tamaño del dataset utilizado, no se debe descartar la posibilidad de haber realizado overfitting. Aun así, las pruebas realizadas han dado resultados satisfactorios. Para futuro es posible realizar un modelo más robusto aumentando la cantidad de muestras y realizando técnicas de bootstrapping.

## Objetivo
Clasificar las enfermedades de dengue, Zika y chikungunya haciendo uso de regresiones logísticas entrenadas a pares con imágenes generadas por CGR.

## Justificación
Las enfermedades causadas por arbovirus poseen prevalencia en zonas tropicales y subtropicales como México y Guatemala. Estos suelen presentarse en forma epidémica y son similares en su expresión clínica, pudiendo desarrollarse desde enfermedades asintomáticas hasta graves con consecuencias como síndromes neuronales (Guillain-Barré) o muerte. La vía de transmisión del virus es por picadura de mosquitos infectados del género Aedes.

## Hipótesis
Utilizar modelos de regresiones logísticas entrenados a pares. Es posible realizar una distinción de las imágenes de las firmas genéticas de los virus estudiados al utilizar una representación del juego de caos específica.

# Diagrama Metodológico

| ![space-1.jpg](doc/diagrama.png) | 
|:--:| 

## Resultados
### Chinkungunya

| ![space-1.jpg](doc/CHIKUNGUNYA-nuccoreEU372006.1.fasta.jpg) | 
|:--:| 
| *Mapa de Calor de la CGR de Chinkungunya* |
    
| ![space-1.jpg](doc/REGRESION_CHIKUNGUNYA.png) | 
|:--:| 
| *Resultado del Modelo para Chinkungunya* |

| ![space-1.jpg](doc/SCATTER_CHIKUNGUNYA.png) | 
|*Scatter Chinkungunya*| 
 
### Dengue
 
| ![space-1.jpg](doc/DENGUE-nuccoreAF309641.1.fasta.jpg) | 
|:--:| 
| *Mapa de Calor de la CGR del Dengue* |
         
| ![space-1.jpg](doc/REGRESION_DENGUE.png) | 
|:--:| 
| *Resultado del Modelo para Dengue* |
 
| ![space-1.jpg](doc/SCATTER_DENGUE.png) | 
|:--:| 
| *Scatter Dengue* |

### Zika

| ![space-1.jpg](doc/DENGUE-nuccoreAF309641.1.fasta.jpg) | 
|:--:| 
| *Mapa de Calor de la CGR de Zika* |
     
| ![space-1.jpg](doc/REGRESION_ZIKA.png) | 
|:--:| 
| *Resultado del Modelo para Zika* |

| ![space-1.jpg](doc/SCATTER_ZIKA.png) | 
|:--:| 
| *Scatter Zika* |

## Conclusión
Las firmas genéticas de los virus utilizados pueden emplearse para la creación de un modelo de CGR y regresión logística el cual permite la clasificación de los virus.

## Referencias
[Sergio Hernández López - Cómo hacer la representación por el juego del caos en python (con Colab)](https://www.youtube.com/watch?v=3m5MGvCX3nI&list=PLE0_iBOs1R0kf4_6D_UyMPFZvgDEKj0rH&index=30)

[Protocolo de Vigilancia Epidemiológica Integrada de Arbovirosis](http://epidemiologia.mspas.gob.gt/files/Publicaciones%202018/Protocolos/Vigilancia%20Epidemiol%C3%B3gica%20Integrada%20de%20Arbovirosis.pdf)

[Chikungunya virus genome](https://www.ncbi.nlm.nih.gov/nuccore/?term=Chikungunya+virus+complete+genome)

[Zika virus genome](https://www.ncbi.nlm.nih.gov/nuccore/?term=zika+complete+genome)

[Dengue virus genome](https://www.ncbi.nlm.nih.gov/nuccore/?term=dengue+virus+complete+genome)